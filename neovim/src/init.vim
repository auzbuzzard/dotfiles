" auto-install vim-plug                                                                                                                
if has("unix") && empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')

Plug 'dense-analysis/ale'
" if has('nvim')
"   Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" else
"   Plug 'Shougo/deoplete.nvim'
"   Plug 'roxma/nvim-yarp'
"   Plug 'roxma/vim-hug-neovim-rpc'
" endif
" let g:deoplete#enable_at_startup = 1

Plug 'tpope/vim-repeat'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-eunuch'
" Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Plug 'deoplete-plugins/deoplete-jedi'
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ervandew/supertab'
" Plug 'sbdchd/vim-run'
" Plug 'liuchengxu/vista.vim'
"Plug 'majutsushi/tagbar'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'flazz/vim-colorschemes'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-obsession'
Plug 'christoomey/vim-tmux-navigator'
" Plug 'chrisbra/csv.vim'
Plug 'janko/vim-test'

Plug 'sbdchd/neoformat'


call plug#end()

syntax enable

filetype on
filetype plugin indent on    " required
filetype plugin on
" Vim Configurations
" Jedi-vim
" let g:jedi#use_splits_not_buffers = "bottom"

set termguicolors
set background=dark
colorscheme monokain
set number
set relativenumber
set linebreak
set display+=lastline
set completeopt-=preview

set mouse=a

set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

" set timeoutlen=125

" Plugin Keymaps
"nmap <F8> :TagbarToggle<CR>
map <C-n> :NERDTreeToggle<CR>
" map <C-n><C-n> :Vista!!<CR>

let g:deoplete#sources#jedi#show_docstring = 1

let g:tmux_navigator_save_on_switch = 2

let g:run_cmd_python = ['python3']
let g:jedi#use_tabs_not_buffers = 0
let g:SuperTabDefaultCompletionType = "<c-n>"

let g:airline_powerline_fonts = 1
let g:airline_theme='minimalist'

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#tab_nr_type = 1
let g:airline#extensions#tabline#show_tab_count = 0

" Ale
let g:ale_lint_on_enter = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_linters = {'python': ['flake8', 'autopep8']}
let g:ale_python_flake8_options = "--max-line-length=120"

" PyTest
let test#strategy = {
    \ 'nearest': 'neovim',
    \ 'file': 'neovim',
    \ 'suite': 'neovim',
\ }

autocmd FileType yaml setlocal tabstop=2 sts=2 shiftwidth=2 expandtab
autocmd FileType mkd setlocal tabstop=2 shiftwidth=2 expandtab

" Vim Key commands
:noremap <PageUp> <C-y>
:noremap <PageDown> <C-e>
:noremap <S-PageUp> <C-U>
:noremap <S-PageDown> <C-D>

:noremap <Up> gk
:noremap <Down> gj

" :nmap <CR><CR> O<Esc>j
" :nmap <CR> o<Esc>k

" vim-test
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>
