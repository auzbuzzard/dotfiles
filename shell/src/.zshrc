### Auto cloning and sourcing Zinit 

ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
	print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
	mkdir -p "$(dirname $ZINIT_HOME)"
	git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME" && \
		print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
		print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "${ZINIT_HOME}/zinit.zsh"

autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

### End of Zinit's installer chunk

### Sources

export PATH=~/.local/bin:$PATH

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

### Themes

zinit ice depth=1; zinit light romkatv/powerlevel10k

### Plugins
# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

zinit light-mode for \
	zsh-users/zsh-autosuggestions \
	zdharma-continuum/fast-syntax-highlighting \
	jocelynmallon/zshmarks
	# marlonrichert/zsh-autocomplete

### Aliases

alias vim="nvim"
alias p3="python3"
case `uname` in
    Darwin)
        export CLICOLOR=Y
    ;;
    Linux)
        alias ls="ls --color=auto"
    ;;
esac
alias ll="ls -alh"
alias la="ls -a"
alias src="source"
alias tnsas="tmux new-session -As "

### Settings

# ensure zsh history is saved
SAVEHIST=1000
HISTFILE=~/.zsh_history
setopt inc_append_history
setopt share_history

# use bat as manpager
if batcat_loc="$(type -p "$bat")" || [[ -z $batcat_loc ]]; then
	export MANPAGER="sh -c 'col -bx | bat -l man -p'"
else
	export MANPAGER=""
fi

# history settings
setopt inc_append_history
setopt share_history
SAVEHIST=1000  # Save most-recent 1000 lines
HISTSIZE=1000
HISTFILE=~/.zsh_history

# tmux autostart
if [[ -z $TMUX ]] && [[ -n $SSH_TTY ]]; then
    exec tmux new-session -A -s main
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

