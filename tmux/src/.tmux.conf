###
# tpm plugins
###

# auto test and install tpm
if "test ! -d ~/.tmux/plugins/tpm" \
   "run 'git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm && ~/.tmux/plugins/tpm/bin/install_plugins'"

set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'

set -g @plugin 'wfxr/tmux-power'

###
# Settings
###

# set 256 color
set -g default-terminal "screen-256color"

# setw -g aggressive-resize off  # for compatibility with iTerm
set-option -g status-position top
set-option -g status-justify left

# vim-resurrect
set -g @resurrect-strategy-nvim 'session'
set -g @continuum-restore 'on'

# panes should count from 1 like the keyboard
set -g base-index 1
setw -g pane-base-index 1

# mouse
set -g mouse on

# vimkeys are superior
set-window-option -g mode-keys vi
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# for new pane in the same dir
bind c new-window      -c "#{pane_current_path}"
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"

###
# Misc
### 

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
